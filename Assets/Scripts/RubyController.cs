﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RubyController : MonoBehaviour
{
    public int maxHealth = 5; //stores Ruby's max possible health. int type means it stores whole numbers
    //putting public in front of the variable makes it accessible from outside the script
    int currentHealth; //stores Ruby's current health
    public float moveSpeed = 3.0f;

    //to allow other scripts to read the value of currentHealth without making it public in order to prevent bugs, use properties.
    public int health {get {return currentHealth;}}//defines the property. The get keyword in the first block gets what's in the second block
    //the second block is just like a normal function, so you just return the currentHealth value.
    //properties are used like variables.
    //you cannot change the variable health in other scripts because you only specified the get operation for health, making it read-only.

    public float timeInvincible = 2.0f;
    bool isInvincible; //stores if currently invinsible or not
    float invincibleTimer; //stores how much time Ruby has left being invincible

    Rigidbody2D rigidbody2d;//creates a new variable rigidbody2d to store the Rigidbody2D and access it from anywhere inside your script

    Animator animator;
    Vector2 lookDirection = new Vector2(0,-1);

    public GameObject projectilePrefab;

    AudioSource audioSource;
    public AudioClip throwSound;
    public AudioClip hitSound;

    // Start is called before the first frame update
    void Start()
    {
        //QualitySettings.vSyncCount=0;
        //Application.targetFrameRate=10;  //makes Unity render 10fps

        //depending on the computer, the mc can move at radically different fps. To fix this use units per second
        //see Time.deltaTime

        rigidbody2d = GetComponent<Rigidbody2D>(); //executed only once at the start
        //asks unity to give you the Rigidbody2D that is attached to Ruby.

        currentHealth = maxHealth; //sets current health to max at the start of the game

        animator = GetComponent<Animator>();

        audioSource= GetComponent<AudioSource>();
        
    }

    public void PlaySound(AudioClip clip)
    {
        audioSource.PlayOneShot(clip);
    }

    // Update is called once per frame
    void Update()
    {
        float horizontal = Input.GetAxis("Horizontal"); //float variable called horizontal which stores the result that Input.GetAxis("Horizontal") provides
        //The word "horizontal" between the parantheses is the parameter: information you give to the function to specify what it should do.
        //Here with "horizontal" the function knows the name of the axis that you want the value of
        float vertical = Input.GetAxis("Vertical");

        Vector2 move = new Vector2(horizontal, vertical);
        
        if(!Mathf.Approximately(move.x, 0.0f) || !Mathf.Approximately(move.y, 0.0f))//checks to see whether move.x or move.y isn't equal to 0
            {
                lookDirection.Set(move.x, move.y);
                lookDirection.Normalize();
            }
        
        animator.SetFloat("Look X", lookDirection.x);
        animator.SetFloat("Look Y", lookDirection.y);
        animator.SetFloat("Speed", move.magnitude);

        Vector2 position = rigidbody2d.position; //Vector2 is a data type that stores the x and y positions of the rigidbody2d
        position = position + move * moveSpeed * Time.deltaTime; // deltaTime is a variable that Unity fills with the time it takes for a frame to be rendered
        rigidbody2d.MovePosition(position); //moves rigidbody2d to the new position

        if (isInvincible)//if Ruby is invincible
        {
            invincibleTimer -= Time.deltaTime; //this effectively counts down time
            if (invincibleTimer < 0) //time is less than 0, invincibility is removed
                isInvincible = false;
        }

        if(Input.GetKeyDown(KeyCode.C))
        {
            Launch();
        }

        if (Input.GetKeyDown(KeyCode.X))
        {
            RaycastHit2D hit = Physics2D.Raycast(rigidbody2d.position + Vector2.up * 0.2f, lookDirection, 1.5f, LayerMask.GetMask("NPC"));
                if (hit.collider != null)
                {
                    NonPlayerCharacter character = hit.collider.GetComponent<NonPlayerCharacter>();
                    if (character != null)
                    {
                        character.DisplayDialog();
                    }  
                }
        }

    }

    public void ChangeHealth(int amount) //function declaration which tells the compiler how to compile that function
    //the function returns void, meaning it doesn't return any value, it just changes the Health.
    //the variable inside the parentheses (int amount) is the parameter, which is the amount of change to the health
    {
        if (amount < 0) //if Ruby's change in health <0 meaning if she's getting hurt
        {
            animator.SetTrigger("Hit");
            if (isInvincible) //if above is true, we check is she's is invincible already
                return;//if above is true, we return because she can't be hurt atm
                //otherwise we make her invincible
            isInvincible = true;
            invincibleTimer = timeInvincible;

            PlaySound(hitSound);
        }

        currentHealth = Mathf.Clamp(currentHealth + amount, 0, maxHealth);//Clampin ensures that the first parameter never goes lower than the second parameter and never above the third.
        //So Ruby's health always stays between 0 and maxHealth.
        UIHealthBar.instance.SetValue(currentHealth/ (float)maxHealth);
    }

    void Launch()
    {
        GameObject projectileObject = Instantiate(projectilePrefab, rigidbody2d.position + Vector2.up * 0.5f, Quaternion.identity);

        Projectile projectile = projectileObject.GetComponent<Projectile>();
        projectile.Launch(lookDirection, 300);

        animator.SetTrigger("Launch");

        PlaySound(throwSound);
    }

}
