﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HealthCollectible : MonoBehaviour
{

    public AudioClip collectedClip;
   void OnTriggerEnter2D(Collider2D other)
   {
       RubyController controller = other.GetComponent<RubyController>();
       //accessed the RubyController component on the GameObject of the Collider that enters the Trigger
       
       if ( controller != null) //!= means not equal while == means is equal
       //this tests whether the value stored in controller is not equal to null: a special value; means nothing/empty
       //if condition is true Unity executes following code, otherwise it skips it
       //the reason for this test is to make sure that only Ruby can get the health
       {
           if (controller.health < controller.maxHealth) //if false health will not be collected
           {
                controller.ChangeHealth(1);//changes the health of Rubycontroller by 1
                Destroy(gameObject);//this is a unity built-in function that destroys whatever you pass to it as a parameter. In this cases the gameobject attached to this script
                controller.PlaySound(collectedClip);
           }
           
       }
   }
}
